// 신청할 수 있는 회원인지 확인하기위한 함수
const checkIsQualifiedMember = (userId) => {
  // sesseion.userId....
  // 유저 아이디를 통해 고유인증번호를 얻은 뒤 active한 회원인지 확인 후 inactive한 회원인 경우 return false 처리
  
  // 신청 사전조건 확인 만족하지 않은 경우 return false 처리

  // 모든 사전 조건 통과 한 경우
  return true
}

// 사용처에서 아래와 같이 사용
// if (checkIsQualifiedMember(userId)) {
//   // 등록 처리
// } else {
//   // 사유와 함께 400 bad request 처리
// }

export default {
    checkIsQualifiedMember,
}
